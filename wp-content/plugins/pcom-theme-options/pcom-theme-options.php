<?php

/**
 * Plugin Name: PontoCom Op&ccedil;&otilde;es do Tema (Blog)
 * Plugin URI: http://agenciadeinternet.com/
 * Description: Op&ccedil;&otilde;es personalizadas para o tema do blog.
 * Author: PontoCom Ag&ecirc;ncia de Internet
 * Author URI: http://agenciadeinternet.com/
**/

//--------------------------------------------------------------------------------------
//	OP��ES DO TEMA
//--------------------------------------------------------------------------------------

	add_action('admin_menu', 'pcom_menu');
    function pcom_menu() {
        add_menu_page('Op&ccedil;&otilde;es do Tema', 'Op&ccedil;&otilde;es do Tema', 'edit_theme_options', 'pcom-theme-options', 'pcom_theme_options');
    }
  
    function pcom_theme_options() {
        global $blog_id;
    
        if (!current_user_can('edit_theme_options'))
            wp_die ( __('You do not have sufficient permissions to access this page.'));

        if (isset($_POST['theme'])){
            foreach ($_POST['theme'] as $k => $v){
                update_option ('_theme_'.$k, $v);
            }
		} ?>
        
		<style>
			.wrap input[type=text]	{ width: 400px; }
			.wrap h3{color: #222; font-size: 1.3em; margin: 1em 0; border-bottom: 1px solid #C0C0C0; width: 620px; background: #DADADA; padding: 7px 13px; box-sizing: border-box; height: 32px;}
		</style>

			<div class="wrap">
				<div class="icon32" id="icon-options-general"><br></div>
				<h2><?php _e('Op&ccedil;&otilde;es do Tema', 'pcom'); ?></h2>
				<form method="POST" action="">

					<h3>Redes Sociais</h3>

					<table class="form-table">

						<tr>
							<th>Facebook</th>
							<td><input type="text" name="theme[facebook]" value="<?php echo get_option ('_theme_facebook'); ?>" /></td>
						</tr>
						<tr>
							<th>Instagram</th>
							<td><input type="text" name="theme[instagram]" value="<?php echo get_option ('_theme_instagram'); ?>" /></td>
						</tr>
						<tr>
							<th>Instagram ID (<a target="_blank" href="http://jelled.com/instagram/lookup-user-id">consulte aqui</a>)</th>
							<td><input type="text" name="theme[instagram_id]" value="<?php echo get_option ('_theme_instagram_id'); ?>" /></td>
						</tr>
						<tr>
							<th>Youtube</th>
							<td><input type="text" name="theme[youtube]" value="<?php echo get_option ('_theme_youtube'); ?>" /></td>
						</tr>
						<tr>
							<th>Pinterest</th>
							<td><input type="text" name="theme[pinterest]" value="<?php echo get_option ('_theme_pinterest'); ?>" /></td>
						</tr>

					</table>

					<h3>Sobre</h3>

					<table class="form-table">

						<tr>
							<th>Endere&ccedil;o</th>
							<td><input type="text" name="theme[sobre_endereco]" value="<?php echo get_option ('_theme_sobre_endereco'); ?>" /></td>
						</tr>

						<tr>
							<th>Telefone</th>
							<td><input type="text" name="theme[sobre_tel]" value="<?php echo get_option ('_theme_sobre_tel'); ?>" /></td>
						</tr>

						<tr>
							<th>Email</th>
							<td><input type="text" name="theme[sobre_email]" value="<?php echo get_option ('_theme_sobre_email'); ?>" /></td>
						</tr>

					</table>

					<h3>Dados SMTP</h3>

					<table class="form-table">
						<tr>
							<th>Host</th>
							<td><input type="text" name="theme[smtp_host]" value="<?php echo get_option ('_theme_smtp_host'); ?>" /></td>
						</tr>
						<tr>
							<th>Porta</th>
							<td><input type="text" name="theme[smtp_port]" value="<?php echo get_option ('_theme_smtp_port'); ?>" /></td>
						</tr>
						<tr>
							<th>Usu&aacute;rio</th>
							<td><input type="text" name="theme[smtp_user]" value="<?php echo get_option ('_theme_smtp_user'); ?>" /></td>
						</tr>
						<tr>
							<th>Senha</th>
							<td><input type="text" name="theme[smtp_password]" value="<?php echo get_option ('_theme_smtp_password'); ?>" /></td>
						</tr>
					</table>

					<h3>Fale Conosco</h3>

					<table class="form-table">
						<tr>
							<th>E-mail do Destinat&aacute;rio</th>
							<td><input type="text" name="theme[email_recebe]" value="<?php echo get_option ('_theme_email_recebe'); ?>" /></td>
						</tr>
					</table>

					<p class="submit">
						<input type="submit" value="Salvar altera&ccedil;&otilde;es" class="button-primary" name="Submit">
					</p>

				</form>
			</div>
			
        <?php
    }


