<?php get_header('02'); ?>

<div class="conteudo-wrap">
    <?php get_sidebar( 'left' ); ?>

    

    <div class="mid-wrap">
        <h1 class="subtit-left"><?php single_cat_title() ?></h1>
        <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post() ?>
            <?php $categorias = get_the_category(); ?>
            <div class="thumb-item <?php echo categoriesBorderColor($categorias, 1); ?>">
            	<div class="thumb-lista-top">
                    <div class="thumb-img"><a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_post_thumbnail('cat_thumb_list') ?></a></div>
                    <div class="thumb-categ"><?php echo categoriesColor($categorias);?></div>
                </div>
                <div class="thumb-lista-bot">
                    <div class="thumb-txt">
                        <div class="thumb-tit"><a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_title() ?></a></div>
                        <div class="thumb-desc"><?php the_excerpt() ?></div>
                    </div>
                    <div class="thumb-data"><a href="<?php the_permalink() ?>"><?php the_time('d-m-Y');?> <span><img src="<?= get_template_directory_uri() ?>/img/postado-icon-02.png" /></span></a></div>
                </div>
            </div>
            <?php endwhile; ?> 

            <div id="last-posts"></div>
            
            <div class="clear"></div>
            <!--<div class="link-vermais"><a href="#" title="Ver mais" class="load-more" id="a-load-more">Ver mais</a></div>-->
        <?php endif; ?>
    </div>
    

    <?php get_sidebar( 'right' ); ?>

    <div class="clear"></div>
</div>


<?php get_footer(); ?>

<script type='text/javascript'>

jQuery(document).ready(function() {

    var pagina = 2;
 
    jQuery('.load-more').on('click', function(e) {
        e.preventDefault();

        var dados_envio = {
            'mais_posts_nonce': js_global.mais_posts_nonce,
            'paged': pagina,
            'action': 'mais_posts'
        }

        jQuery.ajax({
            url: js_global.xhr_url,
            type: 'POST',
            data: dados_envio,
            dataType: 'HTML',
            success: function(response) {
                if (response == '401'  ){
                    console.log('Requisição inválida')
                }
                else if (response == 402) {
                    //console.log('Todos os posts já foram mostrados')
                    $('#a-load-more').html('Fim');
                    $('#a-load-more').hide('slow');
                } else {
                    //console.log(response);
                    $('#last-posts').append(response);
                    pagina = pagina+1;
                }
            }
        });
 
 
    });
 
})

</script>