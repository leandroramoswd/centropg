<?php include("header-02.php") ?>

<style>
#top-02 {background-image:url(img/categ-top-foto.jpg);}
</style>

<div class="conteudo-wrap">
	<?php include("sidebar-left.php") ?>
    
    <div class="post-wrap">
    	<div class="post-social-bar">
        	<p>Compartilhe</p>
            <div class="bar-count"><strong>54</strong><br />Shares</div>
        	<a href="#" title="Compartilhe no Facebook"><img src="img/facebook-bar.png" /><span>30</span></a>
            <a href="#" title="Compartilhe no Twitter"><img src="img/twitter-bar.png" /><span>10</span></a>
            <a href="#" title="Compartilhe no Google+"><img src="img/google+-bar.png" /><span>5</span></a>
            <a href="#" title="Compartilhe no LinkedIn"><img src="img/linkedin-bar.png" /><span>5</span></a>
            <a href="#" title="Envie por Email"><img src="img/mail-bar.png" /><span>4</span></a>
        </div>
        <div class="post-float">
            <div id="post">
                <div class="post-destacada"><img src="img/post-img-g.jpg" /></div>
                <div class="post-top">
                    <span class="post-data">3 horas atrás</span>
                    <h1>Nice & Cool Temporary Tattoos</h1>
                </div>
                <p>Fotografar é enxergar a vida de outra forma.</p>
                <p>A fotografia é essencial para os relacionamentos e para as muitas profissões que valorizam o talento, como as da área da comunicação, da arte e do design. Pela fotografia o olhar criativo desenvolve-se e experimenta o mundo além das possibilidades da técnica.</p>
                <p>O Curso de Fotografia do Centro Europeu capacita seus alunos para o mercado de trabalho, ao mesmo tempo em que os envolve pelo fascínio da arte. Assim, aprende-se a lidar com destreza com os desafios profissionais sem deixar de lado o aprimoramento do olhar artístico de cada um e, esse sim, faz toda a diferença.</p>
                <p>O curso oferece toda estrutura para que os alunos possam conhecer as diversas áreas da fotografia, contando com a experiência e a dedicação dos professores que são profissionais renomados e atuantes em cada área específica. Além de uma supervisão constante, focada no apoio individual e no desenvolvimento da identidade autoral do fotógrafo.</p>
            </div>
            <div class="post-social">
                <p>Compartilhe</p>
                <br />
                <a href="#" title="Compartilhe no Facebook"><img src="img/facebook-bar.png" /></a>
                <a href="#" title="Compartilhe no Twitter"><img src="img/twitter-bar.png" /></a>
                <a href="#" title="Compartilhe no Google+"><img src="img/google+-bar.png" /></a>
                <a href="#" title="Compartilhe no LinkedIn"><img src="img/linkedin-bar.png" /></a>
                <a href="#" title="Envie por Email"><img src="img/mail-bar.png" /></a>
            </div>
            <div class="post-comentario">
            	<p class="p-01">Deixe seu comentário</p>
                
                <div id="fbcomments"><div id="fb-root"></div><script src="http://connect.facebook.net/pt_BR/all.js#xfbml=1"></script><fb:comments href="https://www.facebook.com/zuck/posts/10102735452532991?comment_id=1070233703036185" data-width="100%"></fb:comments></div>
            </div>
        </div>
        
        <div class="clear"></div>
    </div>
    
    <?php include("sidebar-right.php") ?>
    
    <div class="clear"></div>
</div>

<?php include("footer.php") ?>