<?php get_header(); ?>
<body> 
    <div id="page">
        <?php while(have_posts()): the_post() ?>
        <div class="page-title">
            <div class="container">
                <h1><?php the_title() ?></h1>
            </div>
        </div>
        <div class="content">
            <div class="container">
                <div class="col120">
                    <?php the_content() ?>
                </div>
            </div>
        </div>
        <?php endwhile; ?>
<?php get_footer(); ?>