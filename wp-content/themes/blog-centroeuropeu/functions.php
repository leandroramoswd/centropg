<?php

add_theme_support( 'post-thumbnails' );

	function my_theme_setup(){
		load_theme_textdomain('blog-centroeuropeu', get_template_directory());
	}
	add_action('after_setup_theme', 'my_theme_setup');

	// category colors
	function classColor($slug, $thumb = 0) {
		switch ($slug) {
			case 'artes-visuais': 			$classe = 'cat-artes'; 	break;
			case 'cinema': 					$classe = 'cat-cinema'; break;
			case 'design-de-moda': 			$classe = 'cat-moda'; 	break;
			case 'design-de-interiores': 	$classe = 'cat-design'; break;
			case 'empreendedorismo': 		$classe = 'cat-dicas'; 	break;
			case 'fotografia': 				$classe = 'cat-foto'; 	break;
			case 'gastronomia': 			$classe = 'cat-gastro'; break;
			case 'jogos-digitais': 			$classe = 'cat-jogos'; 	break;
			default: 						$classe = 'cat-noticias';break;
		}
		$classe = ($thumb == 1) ? str_ireplace('cat-', 'thumb-', $classe) : $classe;
		return $classe;
	}

	function categoriesColor($categorias, $thumb = 0)
	{
	    foreach ($categorias as $categoria) {
	    	if(($categoria->slug != 'destaque') and ($categoria->slug != 'hot-topic')) {
	        	$link = '<a href="'.get_bloginfo('url').'/categoria/'.$categoria->slug.'" title="'.$categoria->name.'" class="'.classColor($categoria->slug, $thumb).'">' . $categoria->cat_name . '</a>';
	    	}
	    }

	    return $link;
	}

	function categoriesBorderColor($categorias, $thumb = 1)
	{
	    foreach ($categorias as $categoria) {
	    	if(($categoria->slug != 'destaque') and ($categoria->slug != 'hot-topic')) {
	        	$classe = classColor($categoria->slug, $thumb);
	    	}
	    }

	    return $classe;
	}

	function categoriesCurrentID($categorias)
	{
	    foreach ($categorias as $categoria) {
	    	if(($categoria->slug != 'destaque') and ($categoria->slug != 'hot-topic')) {
	        	$result = $categoria;
	    	}
	    }

	    return $result;
	}

	// Thumbs Sizes

		add_image_size('topo_curso', 1920, 192, array('center', 'center'));

		add_image_size('post_thumb_list', 365, 273, array('center', 'center'));
		add_image_size('post_thumb_hot', 230, 172, array('center', 'center'));
		add_image_size('post_thumb_destaque', 320, 310, array('center', 'center'));
		add_image_size('post_thumb_coluna', 103, 103, array('center', 'center'));
		add_image_size('post_thumb_related', 213, 122, array('center', 'center'));

		add_image_size('cat_thumb_list', 222, 166, array('center', 'center'));


	// Functions

		function pageMultisite(){
			$blog_id = get_current_blog_id();

			$blog_list = wp_get_sites(array(
				'network_id' => 0
			));

			global $wpdb;
			$blog_list = $wpdb->get_results( "SELECT * FROM $wpdb->blogs WHERE archived = '0' ORDER BY path" );

			$separador = '|';
			$count = 1;
			$html = '';
			foreach ($blog_list as $blog):
				$blog_details = get_blog_details( $blog->blog_id );
				$html .= '<a href="'.$blog_details->siteurl.'" class="'.($blog_id == $blog_details->blog_id ? 'active' : '').'">'.$blog_details->blogname.'</a>';
				if (end($blog_list) != $blog):
					$html .= '<em>'.$separador.'</em>';
				endif;
				$count++;
			endforeach;
			echo $html;
		}

		function get_ID_by_slug($page_slug) {
		    $page = get_page_by_path($page_slug);
		    if ($page) {
		        return $page->ID;
		    } else {
		        return null;
		    }
		}

		function get_posts_relacionados($id){
			global $post;
			$categories = get_the_category($id);
			$categorias = array();

			foreach($categories as $category) {
				$categorias[] = $category->term_id;
			}

			$args=array(
				'category__in' => $categorias,
				'post__not_in' => array($id),
				'posts_per_page'=> 3,
			);

			$my_query = new WP_Query($args);

			$get_posts_ids = array();
			while ($my_query->have_posts()) {
				$my_query->the_post();
				$get_posts_ids[] = $post->ID;
			}

			wp_reset_query();

			$getPosts = array(
				'post__in' => $get_posts_ids,
				'numberposts'=> 3,
				'post_status' => 'publish',
				'exclude' => $id
			);
			return get_posts($getPosts);
		}

		function get_attachment_id_from_url( $attachment_url ) {
			global $wpdb;
			$attachment_id = false;
			if ( '' == $attachment_url ) return;
			$upload_dir_paths = wp_upload_dir();
			if ( false !== strpos( $attachment_url, $upload_dir_paths['baseurl'] ) ) {
				$attachment_url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url );
				$attachment_url = str_replace( $upload_dir_paths['baseurl'] . '/', '', $attachment_url );
				$attachment_id = $wpdb->get_var( $wpdb->prepare( "SELECT wposts.ID FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta WHERE wposts.ID = wpostmeta.post_id AND wpostmeta.meta_key = '_wp_attached_file' AND wpostmeta.meta_value = '%s' AND wposts.post_type = 'attachment'", $attachment_url ) );
			}
			return $attachment_id;
		}

		function transformDate($data, $separator){
			if($separator = '-') $separator2 = '/';
			else $separator2 = '-';
			$data = explode($separator, $data);
			if(is_home()){
				return $data[2].$separator2.$data[1];
			}else{
            	return $data[2].$separator2.$data[1].$separator2.$data[0];
            }
		}



		function getYoutubeID($url){
			$urls = parse_url($url);
			if ($urls['host'] == 'youtu.be') :
			    return $imgPath = ltrim($urls['path'],'/');
			elseif (strpos($urls['path'],'embed') == 1) :
			    return $imgPath = end(explode('/',$urls['path']));
			elseif (strpos($url,'/') === false):
			    return $imgPath = $url;
			else :
			    parse_str($urls['query']);
			    return $imgPath = $v;
			endif;
		}

		add_action( 'admin_menu', 'my_remove_menu_pages' );
	    function my_remove_menu_pages() {
	        remove_menu_page('edit-comments.php');
	    }

	/********************/
	/* Custom functions */
	/********************/

	function has_parent(){
		global $post;
		return wp_get_post_parent_id($post->ID);
	}


	/********************/
	/*   Custom fields  */
	/********************/


	/*function get_the_post_thumbnail_url($ID){
		$post = get_post(get_post_thumbnail_id($ID));
		return $post->guid;
	}*/

	function get_the_post_thumbnail_size($ID){
		$post = get_post(get_post_thumbnail_id($ID));
		// return $post->guid;
		var_dump($post);
		return '';
	}


/*	// Register Navigation Menus
	function custom_navigation_menus() {

		$locations = array(
			'Dicas' => __( '', 'text_domain' ),
			'Reviews' => __( '', 'text_domain' ),
		);
		register_nav_menus( $locations );

	}
	add_action( 'init', 'custom_navigation_menus' );
*/

	// Register Custom Taxonomy
function pcom_register_taxonomy_secao(){
     register_taxonomy(
          'secao',
          'post',
          array(
               'label' => __('Seção'),
               'rewrite' => array('slug' => 'secao'),
               'hierarchical' => true
          )
     );
}
add_action('init', 'pcom_register_taxonomy_secao');



	/**************************/
	/*     Mais acessados     */
	/**************************/

function pcom_set_post_views($postID) {
    $count_key = 'pcom_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function pcom_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;
    }
    pcom_set_post_views($post_id);
}
add_action( 'wp_head', 'pcom_track_post_views');

// for W3 Total Cache
/*
<!-- mfunc pcom_set_post_views($post_id); --><!-- /mfunc -->
*/

function pcom_get_post_views($postID){
    $count_key = 'pcom_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count;
}


	/**************************/
	/*     Ajax posts         */
	/**************************/

//Adiciona um script para o WordPress
add_action( 'wp_enqueue_scripts', 'secure_enqueue_script' );
function secure_enqueue_script() {
  wp_register_script( 'secure-ajax-access', esc_url( add_query_arg( array( 'js_global' => 1 ), site_url() ) ) );
  wp_enqueue_script( 'secure-ajax-access' );
}

//Joga o nonce e a url para as requisições para dentro do Javascript criado acima
add_action( 'template_redirect', 'javascript_variaveis' );
function javascript_variaveis() {
  if ( !isset( $_GET[ 'js_global' ] ) ) return;

  $nonce = wp_create_nonce('mais_posts_nonce');

  $variaveis_javascript = array(
    'mais_posts_nonce' => $nonce, //Esta função cria um nonce para nossa requisição para buscar mais notícias, por exemplo.
    'xhr_url'             => admin_url('admin-ajax.php') // Forma para pegar a url para as consultas dinamicamente.
  );

  $new_array = array();
  foreach( $variaveis_javascript as $var => $value ) $new_array[] = esc_js( $var ) . " : '" . esc_js( $value ) . "'";

  header("Content-type: application/x-javascript");
  printf('var %s = {%s};', 'js_global', implode( ',', $new_array ) );
  exit;
}



add_action('wp_ajax_nopriv_mais_posts', 'mais_posts');
add_action('wp_ajax_mais_posts', 'mais_posts');

function mais_posts() {
	if( ! wp_verify_nonce( $_POST['mais_posts_nonce'], 'mais_posts_nonce' ) ) {
		echo '401'; // Caso não seja verificado o nonce enviado, a requisição vai retornar 401
    	die();
  	}
  	//Busca os dados que queremos
  	$args = array(
  		//'posts_per_page' => '6',
    	'paged' => $_POST['paged'],
  		'post_type' => ''
  		);
  	$wp_query = new WP_Query( $args  );

  	//Caso tenha os dados, retorna-os / Caso não tenha retorna 402 para tratarmos no frontend
  	if( $wp_query->have_posts() ) {
    	//echo json_encode( $wp_query->posts );
    	$retorno = null;
    	while ( $wp_query->have_posts() ) {
    		$wp_query->the_post();
        	$categorias = get_the_category();
        	$retorno .= '
        	<div class="thumb-item '.categoriesBorderColor($categorias, 1).'">
	        	<div class="thumb-lista-top">
	                <div class="thumb-img"><a href="'.get_the_permalink().'" title="'.get_the_title().'">'.get_the_post_thumbnail('','post_thumb_destaque').'</a></div>
	                <div class="thumb-categ">'.categoriesColor($categorias).'</div>
	            </div>
	            <div class="thumb-lista-bot">
	                <div class="thumb-txt">
	                    <div class="thumb-tit"><a href="'.get_the_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></div>
	                    <div class="thumb-desc">'.get_the_excerpt().'</div>
	                </div>
	                <div class="thumb-data"><a href="'.get_the_permalink().'">'.get_the_time('d-m-Y').' <span><img src="'. get_template_directory_uri().'/img/postado-icon-02.png" /></span></a></div>
	            </div>
	        </div>';
        }



  	} else {
    	$retorno = 402;
  	}
  	echo $retorno;
  	exit;
}


	/**************************/
	/*     Likes, Counts      */
	/**************************/

// RETORNA TOTAL DE FACEBOOK LIKES
// ORIGINAL POR http://halgatewood.com/get-number-of-facebook-likes-for-a-url/
function get_fb_likes($url) {
	$query = "select total_count,like_count,comment_count,share_count,click_count from link_stat where url='{$url}'";

	$call = "https://api.facebook.com/method/fql.query?query=" . rawurlencode($query) . "&format=json";

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $call);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$output = curl_exec($ch);
	curl_close($ch);
	return (int) json_decode($output->like_count);
}

// RETORNA TOTAL DE TWEETS
// ORIGINAL POR http://ottopress.com/2010/twitters-new-tweet-button-and-the-count-api/
function get_tweet_count($url) {
	$count = 0;
	$data = wp_remote_get('ht'.'tp://urls.api.twitter.com/1/urls/count.json?url='.urlencode($url) );
	if (!is_wp_error($data)) {
		$resp = json_decode($data['body'],true);
	if ($resp['count']) $count = $resp['count'];
	}
	return $count;
}

// RETORNA TOTAL DE PLUS
// ORIGINAL POR http://johndyer.name/getting-counts-for-twitter-links-facebook-likesshares-and-google-1-plusones-in-c-or-php/
function get_plusones($url) {
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, "https://clients6.google.com/rpc");
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_POSTFIELDS, '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"' . $url . '","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]');
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
	$curl_results = curl_exec ($curl);
	curl_close ($curl);

	$json = json_decode($curl_results, true);
	return intval( $json[0]['result']['metadata']['globalCounts']['count'] );
}

// RETORNA TOTAL LINKEDIN
function get_linkedin_count($url) {
	$data = wp_remote_get("http://www.linkedin.com/countserv/count/share?url=".$url."&format=json");
	$json = json_decode($data['body'],true);
	return intval( $resp['count'] );
}

function featuredtoRSS($content) {
    global $post;
    if ( has_post_thumbnail( $post->ID ) ){
    $content = '<div>' . get_the_post_thumbnail( $post->ID, 'full', array( 'style' => 'margin-bottom: 15px;' ) ) . '</div>' . $content;
    }
    return $content;
}

add_filter('the_excerpt_rss', 'featuredtoRSS');
add_filter('the_content_feed', 'featuredtoRSS');

function return_3600( $seconds ) {
  // change the default feed cache recreation period to 2 hours
  return 3600;
}

add_filter( 'wp_feed_cache_transient_lifetime' , 'return_7200' );
$feed = fetch_feed( $feed_url );
remove_filter( 'wp_feed_cache_transient_lifetime' , 'return_7200' );