<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Blog Centro Europeu</title>

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="HandheldFriendly" content="true">

    <meta name="title" content="<?php bloginfo('name') ?>" />
    <meta name="description" content="<?php bloginfo('description') ?>" />
    <meta name="keywords" content="<?php bloginfo('description') ?>" />
    <meta name="author" content="Centro Europeu" />
	<meta name="resource-type" content="document" />
    <meta name="distribution" content="Global" />
    <meta name="copyright" content="agenciadeinternet.com" />
    <meta name="robots" content="All" />
    <meta name="rating" content="General" />

    <?php wp_head(); ?>

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,900" rel="stylesheet">

    <link rel="stylesheet" href="<?= bloginfo('stylesheet_url') ?>" type="text/css" />

    <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/swiper/css/swiper.min.css">

    <link rel="shortcut icon" href="<?= get_template_directory_uri() ?>/img/favicon.png" />
    
    <!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-W8VDNT');</script>
	<!-- End Google Tag Manager -->

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W8VDNT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="top">
	<div class="conteudo-wrap">
    	<a href="<?= bloginfo('siteurl') ?>" class="top-logo" title="Centro Europeu"><img src="<?= get_template_directory_uri() ?>/img/ce-logo-top.jpg" /></a>
        <div class="top-meio">
        	<ul id="nav">
            	<li class="link-main"><a href="#" title="Escolha seu mundo">Escolha seu mundoX</a>
                    <?php
                    $args=array(
                    'orderby' => 'name',
                    'order' => 'ASC'
                    );
                    $categories=get_categories($args);

                    /*$menu_categorias = array(
                    'menu' => 'Categorias',
                    'container' => 'ul'
                    );*/

                    //wp_nav_menu($categories);

                    $cats = explode("<br />",wp_list_categories('title_li=&echo=0&depth=1&style=none'));
                    $cat_n = count($cats) - 1;
                    echo "<ul>";
                    for ($i=0;$i< $cat_n;$i++)
                    {
                    echo "<li>$cats[$i]</li>";
                    }
                    echo "</ul>";
                    ?>
                </li>
            </ul>

            <?php
            /*$menu_categorias = array(
            'menu' => 'Categorias',
            'container' => 'ul',
            'menu_id' => 'nav-mobile'
            );
            wp_nav_menu($menu_categorias);*/
            $cats = explode("<br />",wp_list_categories('title_li=&echo=0&depth=1&style=none'));
            $cat_n = count($cats) - 1;
            echo '<ul id="nav-mobile">';
            for ($i=0;$i< $cat_n;$i++)
            {
            echo "<li>$cats[$i]</li>";
            }
            echo '</ul>';
            ?>

            <div class="mobile-btn"><a href="#" title="Clique para expandir" id="mobile-click">Menu</a></div>
        </div>
        <div class="top-busca">
        	<form id="top_busca" method="get" action="<?= bloginfo('url') ?>">
            	<label for="busca-input"></label>
                <input type="text" id="busca-input" name="s" title="O que você procura?" />
            </form>
        </div>
        <div class="top-social">
            <?php if(get_option('_theme_facebook')) : ?>
        	   <a href="https://www.facebook.com/<?php echo get_option ('_theme_facebook'); ?>" title="Facebook"><img src="<?= get_template_directory_uri() ?>/img/facebook-top.png"></a>
            <?php endif; ?>
            <?php if(get_option('_theme_instagram')) : ?>
                <a href="https://www.instagram.com/<?php echo get_option ('_theme_instagram'); ?>" title="Instagram"><img src="<?= get_template_directory_uri() ?>/img/instagram-top.png"></a>
            <?php endif; ?>
            <?php if(get_option('_theme_pinterest')) : ?>
                <a href="https://br.pinterest.com/<?php echo get_option ('_theme_pinterest'); ?>" title="Pinterest"><img src="<?= get_template_directory_uri() ?>/img/pinterest-top.png"></a>
            <?php endif; ?>
            <?php if(get_option('_theme_youtube')) : ?>
                <a href="https://www.youtube.com/user/<?php echo get_option ('_theme_youtube'); ?>" title="Youtube"><img src="<?= get_template_directory_uri() ?>/img/youtube-top.png"></a>
            <?php endif; ?>
            <div class="top-voltar"><a href="http://centroeuropeu.com.br/portal">Voltar ao site</a></div>
        </div>
    	<div class="clear"></div>
    </div>
</div>