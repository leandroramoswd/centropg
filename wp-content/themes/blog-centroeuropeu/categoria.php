<?php include("header-02.php") ?>

<style>
#top-02 {background-image:url(img/categ-top-foto.jpg);}
</style>

<div class="conteudo-wrap">
	<?php include("sidebar-left.php") ?>
    
    <div class="mid-wrap">
    	<div class="thumb-item thumb-dicas">
        	<div class="thumb-lista-top">
                <div class="thumb-img"><a href="#" title="Título"><img src="img/thumb-01.jpg" /></a></div>
                <div class="thumb-categ"><a href="#" title="Categoria" class="cat-dicas">Dicas</a></div>
            </div>
            <div class="thumb-lista-bot">
                <div class="thumb-txt">
                    <div class="thumb-tit"><a href="#" title="Título">Black & White 40’s Photography of San Francisco</a></div>
                    <div class="thumb-desc"><p>Si vous n’avez pas un petit potager mais que vous avez la main verte, Foop est l’engin...</p></div>
                </div>
                <div class="thumb-data"><a href="#">3 horas atrás <span><img src="img/postado-icon-02.png" /></span></a></div>
            </div>
        </div>
        <div class="thumb-item thumb-foto">
        	<div class="thumb-lista-top">
                <div class="thumb-img"><a href="#" title="Título"><img src="img/thumb-02.jpg" /></a></div>
                <div class="thumb-categ"><a href="#" title="Categoria" class="cat-foto">Fotografia</a></div>
            </div>
            <div class="thumb-lista-bot">
                <div class="thumb-txt">
                    <div class="thumb-tit"><a href="#" title="Título">Black & White 40’s Photography of San Francisco</a></div>
                    <div class="thumb-desc"><p>Si vous n’avez pas un petit potager mais que vous avez la main verte, Foop est l’engin...</p></div>
                </div>
                <div class="thumb-data"><a href="#">3 horas atrás <span><img src="img/postado-icon-02.png" /></span></a></div>
            </div>
        </div>
        <div class="thumb-item thumb-cinema">
        	<div class="thumb-lista-top">
                <div class="thumb-img"><a href="#" title="Título"><img src="img/thumb-03.jpg" /></a></div>
                <div class="thumb-categ"><a href="#" title="Categoria" class="cat-cinema">Cinema</a></div>
            </div>
            <div class="thumb-lista-bot">
                <div class="thumb-txt">
                    <div class="thumb-tit"><a href="#" title="Título">Black & White 40’s Photography of San Francisco</a></div>
                    <div class="thumb-desc"><p>Si vous n’avez pas un petit potager mais que vous avez la main verte, Foop est l’engin...</p></div>
                </div>
                <div class="thumb-data"><a href="#">3 horas atrás <span><img src="img/postado-icon-02.png" /></span></a></div>
            </div>
        </div>
        <div class="thumb-item thumb-noticias">
        	<div class="thumb-lista-top">
                <div class="thumb-img"><a href="#" title="Título"><img src="img/thumb-01.jpg" /></a></div>
                <div class="thumb-categ"><a href="#" title="Categoria" class="cat-noticias">Notícias</a></div>
            </div>
            <div class="thumb-lista-bot">
                <div class="thumb-txt">
                    <div class="thumb-tit"><a href="#" title="Título">Black & White 40’s Photography of San Francisco</a></div>
                    <div class="thumb-desc"><p>Si vous n’avez pas un petit potager mais que vous avez la main verte, Foop est l’engin...</p></div>
                </div>
                <div class="thumb-data"><a href="#">3 horas atrás <span><img src="img/postado-icon-02.png" /></span></a></div>
            </div>
        </div>
        <div class="thumb-item thumb-artes">
        	<div class="thumb-lista-top">
                <div class="thumb-img"><a href="#" title="Título"><img src="img/thumb-02.jpg" /></a></div>
                <div class="thumb-categ"><a href="#" title="Categoria" class="cat-artes">Artes Visuais</a></div>
            </div>
            <div class="thumb-lista-bot">
                <div class="thumb-txt">
                    <div class="thumb-tit"><a href="#" title="Título">Black & White 40’s Photography of San Francisco</a></div>
                    <div class="thumb-desc"><p>Si vous n’avez pas un petit potager mais que vous avez la main verte, Foop est l’engin...</p></div>
                </div>
                <div class="thumb-data"><a href="#">3 horas atrás <span><img src="img/postado-icon-02.png" /></span></a></div>
            </div>
        </div>
        <div class="thumb-item thumb-design">
        	<div class="thumb-lista-top">
                <div class="thumb-img"><a href="#" title="Título"><img src="img/thumb-03.jpg" /></a></div>
                <div class="thumb-categ"><a href="#" title="Categoria" class="cat-design">Design de Interiores</a></div>
            </div>
            <div class="thumb-lista-bot">
                <div class="thumb-txt">
                    <div class="thumb-tit"><a href="#" title="Título">Black & White 40’s Photography of San Francisco</a></div>
                    <div class="thumb-desc"><p>Si vous n’avez pas un petit potager mais que vous avez la main verte, Foop est l’engin...</p></div>
                </div>
                <div class="thumb-data"><a href="#">3 horas atrás <span><img src="img/postado-icon-02.png" /></span></a></div>
            </div>
        </div>
        <div class="thumb-item thumb-dicas">
        	<div class="thumb-lista-top">
                <div class="thumb-img"><a href="#" title="Título"><img src="img/thumb-01.jpg" /></a></div>
                <div class="thumb-categ"><a href="#" title="Categoria" class="cat-dicas">Dicas</a></div>
            </div>
            <div class="thumb-lista-bot">
                <div class="thumb-txt">
                    <div class="thumb-tit"><a href="#" title="Título">Black & White 40’s Photography of San Francisco</a></div>
                    <div class="thumb-desc"><p>Si vous n’avez pas un petit potager mais que vous avez la main verte, Foop est l’engin...</p></div>
                </div>
                <div class="thumb-data"><a href="#">3 horas atrás <span><img src="img/postado-icon-02.png" /></span></a></div>
            </div>
        </div>
        <div class="thumb-item thumb-foto">
        	<div class="thumb-lista-top">
                <div class="thumb-img"><a href="#" title="Título"><img src="img/thumb-02.jpg" /></a></div>
                <div class="thumb-categ"><a href="#" title="Categoria" class="cat-foto">Fotografia</a></div>
            </div>
            <div class="thumb-lista-bot">
                <div class="thumb-txt">
                    <div class="thumb-tit"><a href="#" title="Título">Black & White 40’s Photography of San Francisco</a></div>
                    <div class="thumb-desc"><p>Si vous n’avez pas un petit potager mais que vous avez la main verte, Foop est l’engin...</p></div>
                </div>
                <div class="thumb-data"><a href="#">3 horas atrás <span><img src="img/postado-icon-02.png" /></span></a></div>
            </div>
        </div>
        <div class="thumb-item thumb-cinema">
        	<div class="thumb-lista-top">
                <div class="thumb-img"><a href="#" title="Título"><img src="img/thumb-03.jpg" /></a></div>
                <div class="thumb-categ"><a href="#" title="Categoria" class="cat-cinema">Cinema</a></div>
            </div>
            <div class="thumb-lista-bot">
                <div class="thumb-txt">
                    <div class="thumb-tit"><a href="#" title="Título">Black & White 40’s Photography of San Francisco</a></div>
                    <div class="thumb-desc"><p>Si vous n’avez pas un petit potager mais que vous avez la main verte, Foop est l’engin...</p></div>
                </div>
                <div class="thumb-data"><a href="#">3 horas atrás <span><img src="img/postado-icon-02.png" /></span></a></div>
            </div>
        </div>
        <div class="thumb-item thumb-noticias">
        	<div class="thumb-lista-top">
                <div class="thumb-img"><a href="#" title="Título"><img src="img/thumb-01.jpg" /></a></div>
                <div class="thumb-categ"><a href="#" title="Categoria" class="cat-noticias">Notícias</a></div>
            </div>
            <div class="thumb-lista-bot">
                <div class="thumb-txt">
                    <div class="thumb-tit"><a href="#" title="Título">Black & White 40’s Photography of San Francisco</a></div>
                    <div class="thumb-desc"><p>Si vous n’avez pas un petit potager mais que vous avez la main verte, Foop est l’engin...</p></div>
                </div>
                <div class="thumb-data"><a href="#">3 horas atrás <span><img src="img/postado-icon-02.png" /></span></a></div>
            </div>
        </div>
        <div class="thumb-item thumb-artes">
        	<div class="thumb-lista-top">
                <div class="thumb-img"><a href="#" title="Título"><img src="img/thumb-02.jpg" /></a></div>
                <div class="thumb-categ"><a href="#" title="Categoria" class="cat-artes">Artes Visuais</a></div>
            </div>
            <div class="thumb-lista-bot">
                <div class="thumb-txt">
                    <div class="thumb-tit"><a href="#" title="Título">Black & White 40’s Photography of San Francisco</a></div>
                    <div class="thumb-desc"><p>Si vous n’avez pas un petit potager mais que vous avez la main verte, Foop est l’engin...</p></div>
                </div>
                <div class="thumb-data"><a href="#">3 horas atrás <span><img src="img/postado-icon-02.png" /></span></a></div>
            </div>
        </div>
        <div class="thumb-item thumb-design">
        	<div class="thumb-lista-top">
                <div class="thumb-img"><a href="#" title="Título"><img src="img/thumb-03.jpg" /></a></div>
                <div class="thumb-categ"><a href="#" title="Categoria" class="cat-design">Design de Interiores</a></div>
            </div>
            <div class="thumb-lista-bot">
                <div class="thumb-txt">
                    <div class="thumb-tit"><a href="#" title="Título">Black & White 40’s Photography of San Francisco</a></div>
                    <div class="thumb-desc"><p>Si vous n’avez pas un petit potager mais que vous avez la main verte, Foop est l’engin...</p></div>
                </div>
                <div class="thumb-data"><a href="#">3 horas atrás <span><img src="img/postado-icon-02.png" /></span></a></div>
            </div>
        </div>
        
        
    	<div class="clear"></div>
        <br />
        <div class="link-vermais"><a href="#" title="Ver mais">Ver mais</a></div>
    </div>
    
    <?php include("sidebar-right.php") ?>
    
    <div class="clear"></div>
</div>

<?php include("footer.php") ?>