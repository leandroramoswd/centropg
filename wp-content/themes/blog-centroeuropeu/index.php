<?php get_header(); ?>

<?php
$query = new WP_Query(array( 
    'category_name' => 'destaque',
    'orderby' => 'rand',
    'posts_per_page' => '4'
    ));
?>
<?php if ( $query->have_posts() ) : ?>
<div class="box-destaques">
	<div class="conteudo-wrap">
    	<ul>
            <?php while ( $query->have_posts() ) : $query->the_post() ?>
            <li class="dest-item">
            	<div class="dest-img"><a href="<?php the_permalink() ?>"><?php the_post_thumbnail('post_thumb_destaque') ?></a></div>
                <div class="dest-mask"></div>
                <div class="dest-txt">
                    <div class="dest-categ">
                    <?php 
                    $categorias = get_the_category();
                    echo categoriesColor($categorias);
                    //print_r($categorias);
                    ?></div>
                    <div class="dest-tit"><a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_title() ?></a></div>
                    <div class="dest-breve"><?php the_excerpt() ?></div>
                    <div class="dest-data"><a href="<?php the_permalink() ?>"><?php the_time('d-m-Y');?><span><img src="<?= get_template_directory_uri() ?>/img/postado-icon.png" /></span></a></div>
                </div>
            </li>
            <?php endwhile; ?>            
            <div class="clear"></div>
        </ul>
    </div>
</div>
<?php endif; ?>

<?php
$query = new WP_Query(array( 
    //'category_name' => 'hot-topic',
    //'orderby' => 'rand',
    'posts_per_page' => '20',
    'meta_key' => 'pcom_post_views_count', 
    'orderby' => 'meta_value_num', 
    'order' => 'DESC'
    ));
?>
<?php if ( $query->have_posts() ) : ?>
<div class="hot-topics">
	<h1 class="subtit-left">Hot Topics</h1>
    <div class="hot-wrap">
    	<a href="#" class="slider-left" title="Anteriores"></a>
        <div class="slider-mid swiper-container-initial">

            <div class="swiper-wrapper">
            <?php while ( $query->have_posts() ) : $query->the_post() ?>
            	<div class="hot-item swiper-slide">
                	<div class="hot-img"><a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_post_thumbnail('post_thumb_hot') ?></a></div>
                    <div class="hot-txt">
                        <div class="hot-categ">
                            <?php 
                            $categorias = get_the_category();
                            echo categoriesColor($categorias);
                            ?>
                        </div>
                        <div class="hot-tit"><a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_title() ?></a></div>
                        <div class="hot-data"><a href="<?php the_permalink() ?>"><span><img src="<?= get_template_directory_uri() ?>/img/postado-icon-02.png" alt="cliques" class="hot-img-click" /></span> <?php echo pcom_get_post_views(get_the_ID()); ?></a></div>
                    </div>
                </div>
            <?php endwhile; ?>  
            </div>

            
        </div>
        <a href="#" class="slider-right" title="Próximos"></a>
        <div class="clear"></div>
    </div>
</div>
<?php endif; ?>

<?php
$query = new WP_Query(array( 
    'posts_per_page' => '6'
    ));
?>
<?php if ( $query->have_posts() ) : ?>

<div class="thumb-lista-home">
    <div class="thumb-lista">
        <?php while ( $query->have_posts() ) : $query->the_post() ?>
        <?php $categorias = get_the_category(); ?>
        <div class="thumb-item <?php echo categoriesBorderColor($categorias, 1); ?>">
        	<div class="thumb-lista-top">
                <div class="thumb-img"><a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_post_thumbnail('post_thumb_destaque') ?></a></div>
                <div class="thumb-categ"><?php echo categoriesColor($categorias);?></div>
            </div>
            <div class="thumb-lista-bot">
                <div class="thumb-txt">
                    <div class="thumb-tit"><a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_title() ?></a></div>
                    <div class="thumb-desc"><?php the_excerpt() ?></div>
                </div>
                <div class="thumb-data"><a href="<?php the_permalink() ?>"><?php the_time('d-m-Y');?> <span><img src="<?= get_template_directory_uri() ?>/img/postado-icon-02.png" /></span></a></div>
            </div>
        </div>
        <?php endwhile; ?> 

        <div id="last-posts"></div>
        
        <div class="clear"></div>
        <div class="link-vermais"><a href="#" title="Ver mais" class="load-more" id="a-load-more">Ver mais</a></div>
    </div>
</div>
<?php endif; ?>


<?php get_footer(); ?>



<script type='text/javascript'>

jQuery(document).ready(function() {

    var pagina = 2;
 
    jQuery('.load-more').on('click', function(e) {
        e.preventDefault();

        var dados_envio = {
            'mais_posts_nonce': js_global.mais_posts_nonce,
            'paged': pagina,
            'action': 'mais_posts'
        }

        jQuery.ajax({
            url: js_global.xhr_url,
            type: 'POST',
            data: dados_envio,
            dataType: 'HTML',
            success: function(response) {
                if (response == '401'  ){
                    console.log('Requisição inválida')
                }
                else if (response == 402) {
                    //console.log('Todos os posts já foram mostrados')
                    $('#a-load-more').html('Fim');
                    $('#a-load-more').hide('slow');
                } else {
                    //console.log(response);
                    $('#last-posts').append(response);
                    pagina = pagina+1;
                }
            }
        });
 
 
    });
 
})

</script>