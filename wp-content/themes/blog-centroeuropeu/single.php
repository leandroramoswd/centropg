<?php get_header('02'); ?>

<div class="conteudo-wrap">
	<?php get_sidebar( 'left' ); ?>


	<div class="post-wrap">
    	<div class="post-social-bar">
    		<?php 
    		$url = get_permalink(); 
    		if(function_exists('get_fb_likes')) {				
				$fb_count = get_fb_likes("$url");
				$total_fb = $fb_count;
			}
			if(function_exists('get_tweet_count')) {				
				$tw_count = get_tweet_count("$url");
				$total_tw = $tw_count;
			}
			if(function_exists('get_plusones')) {				
				$gl_count = get_plusones("$url");
				$total_gl = $gl_count;
			}
			if(function_exists('get_linkedin_count')) {				
				$ld_count = get_linkedin_count("$url");
				$total_ld = $ld_count;
			}
			$total_share = $total_fb+$total_tw+$total_gl+$total_ld;

    		?>
        	<p>Compartilhe</p>
            <div class="bar-count"><strong><?=$total_share?></strong><br />Shares</div>
        	<a href="https://www.facebook.com/sharer/sharer.php?u=<?=$url?>" title="Compartilhe no Facebook" target="_blank"><img src="<?php bloginfo('stylesheet_directory')?>/img/facebook-bar.png" />
        	<?php echo '<span>'.$total_fb.'</span>';?>
        	</a>
            <a href="https://twitter.com/intent/tweet?url=<?=$url?>" title="Compartilhe no Twitter" target="_blank"><img src="<?php bloginfo('stylesheet_directory')?>/img/twitter-bar.png" />
            <?php echo '<span>'.$total_tw.'</span>';?>
            </a>
            <a href="https://plus.google.com/share?url=<?=$url?>" title="Compartilhe no Google+" target="_blank"><img src="<?php bloginfo('stylesheet_directory')?>/img/google+-bar.png" />
            <?php echo '<span>'.$total_gl.'</span>';?>
            </a>
            <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?=$url?>&title=<?php the_title();?>" title="Compartilhe no LinkedIn" target="_blank"><img src="<?php bloginfo('stylesheet_directory')?>/img/linkedin-bar.png" />
            <?php echo '<span>'.$total_ld.'</span>';?>
			</a>
            <!--<a href="#" title="Envie por Email"><img src="<?php bloginfo('stylesheet_directory')?>/img/mail-bar.png" /><span>4</span></a>-->
        </div>
        <div class="post-float">
            <div id="post">
                <div class="post-destacada"><?php the_post_thumbnail() ?></div>
                <div class="post-top">
                    <span class="post-data"><?php echo get_the_date(); ?></span>
                    <h1><?php the_title();?></h1>
                </div>
                <?php the_excerpt();?>

                <br>
                <?php
                if ( have_posts() ) {
                	while ( have_posts() ) {
                		the_post();
				  		the_content();
					}
				}
				?>
                

            </div>
            <div class="post-social">
                <p>Compartilhe:</p>
                <br />
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?=$url?>" title="Compartilhe no Facebook" target="_blank"><img src="<?php bloginfo('stylesheet_directory')?>/img/facebook-bar.png" /></a>
                <a href="https://twitter.com/intent/tweet?url=<?=$url?>" title="Compartilhe no Twitter" target="_blank"><img src="<?php bloginfo('stylesheet_directory')?>/img/twitter-bar.png" /></a>
                <a href="https://plus.google.com/share?url=<?=$url?>" title="Compartilhe no Google+" target="_blank"><img src="<?php bloginfo('stylesheet_directory')?>/img/google+-bar.png" /></a>
                <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?=$url?>&title=<?php the_title();?>" title="Compartilhe no LinkedIn" target="_blank"><img src="<?php bloginfo('stylesheet_directory')?>/img/linkedin-bar.png" /></a>
                <!--<a href="#" title="Envie por Email"><img src="<?php bloginfo('stylesheet_directory')?>/img/mail-bar.png" /></a>-->
            </div>
            <div class="post-comentario">
            	<p class="p-01">Deixe seu comentário</p>
                
                <div id="fbcomments"><div id="fb-root"></div><script src="http://connect.facebook.net/pt_BR/all.js#xfbml=1"></script><fb:comments href="<?php the_permalink() ?>" data-width="100%"></fb:comments></div>
            </div>
        </div>
        
        <div class="clear"></div>
    </div>
    
    <?php get_sidebar( 'right' ); ?>
    
    <div class="clear"></div>
</div>


<?php get_footer(); ?>

