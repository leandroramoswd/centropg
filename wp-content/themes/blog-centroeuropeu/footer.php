<div id="footer">
	<div class="footer-top">
    	<ul class="footer-menu">
        	<li class="li-tit">Cursos</li>
            <li><a href="http://centroeuropeu.com.br/portal/cursos-de-profissoes/" title="Profissões">Profissões</a></li>
            <li><a href="http://centroeuropeu.com.br/portal/idiomas/" title="Idiomas">Idiomas</a></li>
            <li><a href="http://centroeuropeu.com.br/portal/academia-de-direito/" title="Academia de Direito">Academia de Direito</a></li>
            <li><a href="http://centroeuropeu.com.br/portal/cursos-de-especializacoes/" title="Especializações">Especializações</a></li>
            <li><a href="http://centroeuropeu.com.br/portal/musica-eletronica/" title="Música Eletrônica">Música Eletrônica</a></li>
        </ul>
        <ul class="footer-menu">
        	<li class="li-tit">Sedes</li>
            <li><a href="http://centroeuropeu.com.br/portal/" title="Curitiba">Curitiba</a></li>
            <li><a href="http://centroeuropeu.com.br/portal/pontagrossa/" title="Ponta Grossa">Ponta Grossa</a></li>
            <li><a href="http://centroeuropeu.com.br/portal/joinville/" title="Joinville">Joinville</a></li>
            <li><a href="http://www.hotelcentroeuropeutourist.com.br/" title="Hotel Centro Europeu">Hotel Centro Europeu</a></li>
        </ul>
        <ul class="footer-menu">
        	<li class="li-tit">Centro Europeu</li>
            <li><a href="http://centroeuropeu.com.br/portal/sobre/contato/" title="Fale Conosco">Fale Conosco</a></li>
            <li><a href="http://centroeuropeu.com.br/portal/sobre/trabalhe-conosco/" title="Trabalhe Conosco">Trabalhe Conosco</a></li>
            <li><a href="http://centroeuropeu.com.br/portal/sobre/franquias/" title="Franquias">Franquias</a></li>
            <li><a href="http://centroeuropeu.com.br/portal/sobre/parceiros/" title="Parceiros">Parceiros</a></li>
            <li><a href="http://centroeuropeu.com.br/portal/sobre/" title="Conheça">Conheça</a></li>
        </ul>
        <div class="footer-social">
            <?php if(get_option('_theme_facebook')) : ?>
               <a href="https://www.facebook.com/<?php echo get_option ('_theme_facebook'); ?>" title="Facebook"><img alt="facebook" src="<?php bloginfo('stylesheet_directory')?>/img/facebook-rdp.png"></a>
            <?php endif; ?>
            <?php if(get_option('_theme_instagram')) : ?>
                <a href="https://www.instagram.com/<?php echo get_option ('_theme_instagram'); ?>" title="Instagram"><img alt="instagram" src="<?php bloginfo('stylesheet_directory')?>/img/instagram-rdp.png"></a>
            <?php endif; ?>
            <?php if(get_option('_theme_pinterest')) : ?>
                <a href="https://br.pinterest.com/<?php echo get_option ('_theme_pinterest'); ?>" title="Pinterest"><img alt="P" src="<?php bloginfo('stylesheet_directory')?>/img/flikr-rdp.png"></a>
            <?php endif; ?>
            <?php if(get_option('_theme_youtube')) : ?>
                <a href="https://www.youtube.com/user/<?php echo get_option ('_theme_youtube'); ?>" title="Youtube"><img alt="youtube" src="<?php bloginfo('stylesheet_directory')?>/img/youtube-rdp.png"></a>
            <?php endif; ?>
        </div>
    </div>
    <div class="footer-mid">
    	<p>
            <?php echo get_option ('_theme_sobre_endereco'); ?> |
            <?php echo get_option ('_theme_sobre_tel'); ?> |
            <a href="mailto:<?php echo get_option ('_theme_sobre_email'); ?>?subject=Centro%20Europeu"><?php echo get_option ('_theme_sobre_email'); ?></a>
        </p>
    </div>
    <div class="footer-bot">
    	<a href="http://pontocom.ag" target="_blank" class="pcom">
        	<span class="pb"><img src="<?php bloginfo('stylesheet_directory')?>/img/powered-by.png"></span>
            <span class="color"><img src="<?php bloginfo('stylesheet_directory')?>/img/powered-by-h.png"></span>
        </a>
    </div>
</div>


<script type="text/javascript" src="<?php bloginfo('stylesheet_directory')?>/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory')?>/swiper/js/swiper.jquery.min.js"></script>


<script>
$(document).ready(function(){
	$("#mobile-click").on("click", function(){
		$("#nav-mobile").slideToggle();
		$(this).toggleClass("active");
	});
	
	$('#nav-mobile li').on('click', function(){
        $("#nav-mobile").hide();
        $("#menu-click").removeClass("active");
    });

});
</script>

<script>        
  var sidebarRight = new Swiper ('.swiper-container-right', {
    // Optional parameters
    direction: 'vertical',
    loop: false,
    slidesPerView: 4,
    freeMode: true,
    spaceBetween: 10,
    // Navigation arrows
    nextButton: '.sl-button-next',
    prevButton: '.sl-button-prev',
    

  })     

  var initialHotTopic = new Swiper ('.swiper-container-initial', {
    // Optional parameters
    direction: 'horizontal',
    loop: false,
    slidesPerView: 4,
    spaceBetween: 0,
    // Navigation arrows
    nextButton: '.slider-right',
    prevButton: '.slider-left',
    breakpoints: {
        // when window width is <= 320px
        646: {
          slidesPerView: 1
        },
        // when window width is <= 480px
        1170: {
          slidesPerView: 2
        }
    }
    

  })        
  </script>


<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

</body>
</html>