<div class="sidebar-right">
	<div class="todos-artigos"><a href="<?= bloginfo('siteurl') ?>" title="Todos os Artigos">Todos os Artigos</a></div>
    

    <?php
    $query = new WP_Query(array( 
        'posts_per_page' => '20',
        'meta_key' => 'pcom_post_views_count', 
        'orderby' => 'meta_value_num', 
        'order' => 'DESC'
        ));
    ?>
    <?php if ( $query->have_posts() ) : ?>
    
    <div class="hot-sidebar" style="margin-top: 10px;">

        <div class="hot-sidebar-wrap">

        	<div class="hot-filter" style="top:-18px;">
            	<div>
            	<!--<a href="#" title="Semana" class="active">Semana</a>
                <a href="#" title="Mês">Mês</a>-->
                <h1>Hot topics</h1>
                </div>
            </div>

            <div class="swiper-container-right">

                <div class="swiper-wrapper">

                    <?php while ( $query->have_posts() ) : $query->the_post() ?>
                    <div class="hot-item swiper-slide">
                        <div class="hot-img"><a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_post_thumbnail('post_thumb_coluna') ?></a></div>
                        <div class="hot-txt">
                            <div class="hot-categ">
                                <?php 
                                $categorias = get_the_category();
                                echo categoriesColor($categorias);
                                ?>
                            </div>
                            <div class="hot-tit"><a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_title() ?></a></div>
                            <div class="hot-data"><a href="<?php the_permalink() ?>"><span><img src="<?= get_template_directory_uri() ?>/img/postado-icon-02.png" alt="cliques" class="hot-img-click" /></span> <?php the_time('d-m-Y');?></a></div>
                        </div>

                    </div>
                    <?php endwhile; ?>
                </div>               

            </div>
            <div class="sidebar-updown">
                    <a href="#" class="sl-button-next"><img src="<?= get_template_directory_uri() ?>/img/submenu-seta-down.png"></a>
                    <a href="#" class="sl-button-prev"><img src="<?= get_template_directory_uri() ?>/img/submenu-seta-up.png"></a>
                </div>

        </div>
    </div>

    <?php endif; ?>

</div>