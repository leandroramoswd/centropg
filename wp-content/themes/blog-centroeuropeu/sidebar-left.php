<div class="sidebar-left">
	<div class="sidebar-topzinho">
    	<a href="http://centroeuropeu.com.br/portal/sobre/" title="Sobre">Sobre</a>
        <a href="http://centroeuropeu.com.br/portal/sobre/contato/" title="Contato">Contato</a>
    </div>
    <div class="sidebar-nav">

        <?php
        $categorias = get_the_category();
        if ($categorias) {
            $cat = categoriesCurrentID($categorias);
            $left_title = $cat->name;
            $left_slug = $cat->slug;
        }
        //print_r($cat);
        ?>

        <ul>
            <li><a href="<?= bloginfo('siteurl') ?>/<?php echo $left_slug; ?>" title="<?php echo $left_title; ?>" class="ico-home active">Início<span></span></a></li>
        </ul>
            <?php
                $menu_categorias = array(
                'menu' => 'Secoes',
                'container' => 'ul',
                'menu_class' => ''
                );
                wp_nav_menu($menu_categorias);
            ?>

    </div>
</div>