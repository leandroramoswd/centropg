<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa user o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'centroeuropeu_blog');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'A&HY$7G0SKnAf05z');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>V:y)UHCfwW*T9fhX)=(&>{:Kq_g~md2j&kbOB{]mz8+=Z`Be0<xSDXRe2U0Q,tH');
define('SECURE_AUTH_KEY',  '(M#+Nnb!~3kgr7}do@/iMOyU|s~[LQCDXpq|0-}Eba4*vw:{R}|v`Q IVm{}IBv&');
define('LOGGED_IN_KEY',    'rF3JNA{N^zE$*XL3iPLriE|-jsb)e1%Z.NqNEH&NMQD(u/icDto0qd2)wakS=Uop');
define('NONCE_KEY',        'M_*IsEi:#za.Yo|)+tZ6E..v)+>sz}0JDvQV8?1/F5 %}SrtBN@N*lLI!z<?e:Th');
define('AUTH_SALT',        'a{+s:!V|/THu0K@ppX&A9`]SqVlYxTZgc=FR}bk&<ZhP4?H fJH9J_$Whwth.Za`');
define('SECURE_AUTH_SALT', 'uS[(q<8=L4l+OXKwn&>#q7,;^]Ubgc~$s2o}%B!&.,}q#iUAitJDeN71tp7C?v(>');
define('LOGGED_IN_SALT',   'UtA(BHUz6&z~3Q-j(>/>c:{T *&p0[GlzvKKP:&,[:VD-ZgOPbcQ^^p]jq+UbeNV');
define('NONCE_SALT',       '=q]0=THcY_;i|]Ka+qP_v~Lz:c7(j0~U0eBYJxqz9gN = r[yW[P4L=n+Vcmgr1-');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * para cada um um único prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);
//define('FS_METHOD','direct');

/*define('WP_ALLOW_MULTISITE', true);
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'localhost.centro-europeu');
define('PATH_CURRENT_SITE', '/blog/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);*/
/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
